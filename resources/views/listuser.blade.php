<html>
	<body>
		<table border="1">
			<thead>
				<th>No.</th>
				<th>username</th>
				<th>address</th>
				<th>gender</th>
				<th>birth</th>
			</thead>
			<tbody>
				<?php $i = 1; ?>
				@foreach($data as $row)
				<tr onclick="location.href = '{{route('user.show', $row['_id'])}}'" style="cursor: pointer">
					<td>{{$i}}</td>
					<td>{{$row['username']}}</td>
					<td>{{$row['address']['street']}}</td>
					<td>{{$row['personal_information']['gender']}}</td>
					<td>{{$row['personal_information']['birth']}}</td>
				</tr>
				<?php $i++; ?>
				@endforeach
			</tbody>
		</table>
	</body>
</html>