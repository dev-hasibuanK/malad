<?php

namespace App\Http\Controllers\Api\Guest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Storage;

class Guest extends Controller
{
    //
    public function store(Request $request)
    {
    	# code...    	
    	$validator = \Validator::make($request->all(), [
            'name' => 'required|alpha',
            'email' => 'required|email',
            'phone_number' => 'required|numeric',
        ]);        

    	if ($validator->fails()) {
    		$errors = $validator->errors();
    		return $errors->toJson();
    	}

    	$store = \App\guest::create([
    		'name' => $request->input('name'),
    		'email' => $request->input('email'),
    		'phone_number' => $request->input('phone_number'),
    	]);

    	return response()
            ->json(["msg" => "succes"]);
    }

    public function stoteImage(Request $request)
    {
    	# code...
    	$url = $request->input('url');

    	$ch = curl_init();
    	// Will return the response, if false it print the response
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	// Set the url
    	curl_setopt($ch, CURLOPT_URL,$url);
    	// Execute
    	$result=curl_exec($ch);
    	// Closing
    	curl_close($ch);

    	// Will dump a beauty json :3
    	// var_dump(json_decode($result, true));
    	$response = json_decode($result, true);

// 		
		$url = $response['image'];
		$contents = file_get_contents($url);
		$name = substr($url, strrpos($url, '/') + 1);
		Storage::put($name, $contents);
    	// return substr($url, strrpos($url, '/') +1);

    	return $response;


    }
}
