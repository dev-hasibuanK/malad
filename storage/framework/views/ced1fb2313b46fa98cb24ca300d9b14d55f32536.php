<html>
	<body>
		<table border="1">
			<thead>
				<th>No.</th>
				<th>username</th>
				<th>address</th>
				<th>gender</th>
				<th>birth</th>
			</thead>
			<tbody>
				<?php $i = 1; ?>
				<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr onclick="location.href = '<?php echo e(route('user.show', $row['_id'])); ?>'" style="cursor: pointer">
					<td><?php echo e($i); ?></td>
					<td><?php echo e($row['username']); ?></td>
					<td><?php echo e($row['address']['street']); ?></td>
					<td><?php echo e($row['personal_information']['gender']); ?></td>
					<td><?php echo e($row['personal_information']['birth']); ?></td>
				</tr>
				<?php $i++; ?>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</tbody>
		</table>
	</body>
</html>