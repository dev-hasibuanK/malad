<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Faker\Generator as Faker;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function (Faker $faker) {
	// $loop = 10000;	
	// // $del = DB::connection('mongodb')->collection('mycol')->delete();

	// for ($i=0; $i < $loop; $i++) { 
	// 	# code...
	// 	$data = DB::connection('mongodb')->collection('mycol')->insert(
	// 		[
	// 			'username' => $faker->userName,
	// 			'firstname' => $faker->firstName,
	// 			'lastname' => $faker->lastName,
	// 			'address' => [
	// 				'street' => $faker->streetAddress,					
	// 				'city' => $faker->city,
	// 				'state' => $faker->state,
	// 				'poscode' => $faker->postcode,

	// 			],
	// 			'personal_information' => [
	// 				'no_id' => $faker->randomNumber($nbDigits = NULL, $strict = false),
	// 				'gender' => $faker->randomElement($array = array ('male','female')),
	// 				'birth' => $faker->dateTimeThisCentury->format('Y-m-d'),
	// 			],
	// 		]);
	// }
	
	// $data = DB::connection('mongodb')->collection('mycol')->get();

 //    return $data;
});

Route::get('/users', function ()
{
	# code...
	$data = DB::connection('mongodb')->collection('mycol')->get();
	return \view('listuser', ['data'=>$data]);
})->name('user.index');

Route::get('/user/{id}', function ($id)
{
	# code...
	$data = DB::connection('mongodb')->collection('mycol')->where('_id', $id)->first();
	return \view('showuser', ['data'=>$data]);
})->name('user.show');


// Route::get('/stoteUser', function ()
// {
// 	# code...
// 	return "cds";
	
// })->name('user.store');

